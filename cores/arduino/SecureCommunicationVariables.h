#ifndef SECURE_COMMUNICATION_VARIABLES_H
# define SECURE_COMMUNICATION_VARIABLES_H
# ifdef __AVR__
#  include <avr/pgmspace.h>
# endif /* __AVR__ */
# include <stdlib.h>
# include <stdint.h>
# ifndef PROGMEM
#  define PROGMEM
# endif /* PROGMEM */
# ifdef SECURE_COMMUNICATION_PSK
extern const uint8_t _encryption_key[] PROGMEM;
inline int getEncryptionKey(uint8_t *);
# endif /* SECURE_COMMUNICATION_PSK */
extern const uint8_t _encryption_public_key[] PROGMEM;
extern const uint8_t _encryption_private_key[] PROGMEM;

int getPublicKey(uint8_t *);
int getPrivateKey(uint8_t *);
#endif /* SECURE_COMMUNICATION_VARIABLES_H */