#ifndef SECURE_STREAM_H
# if defined(PROXY_PRINT) && PROXY_PRINT == 1 && defined(PROXY_STREAM) && PROXY_STREAM == 1
#  define SECURE_STREAM_H
#  define BLOCK_SIZE                 16
//#  define SYNC_PATTERN               0x2a
#  define SYNC_PATTERN               'a'
#  define LENGTH_MASK                0x0f
#  define PROTOCOL_LEVEL_TRANSPORT   0
#  define PROTOCOL_LEVEL_USER        1
#  ifdef __AVR__
extern "C" {
    void _init_seed(void);
}
#  endif /* __AVR__ */
#  include <stdint.h>
#  include <stddef.h>
#  include "Stream.h"

class SecureStream : public Stream {
public:
    struct BlockHeader {
        uint8_t data_length : 4;
        uint8_t protocol_level : 1;
        uint8_t key_update : 1;
        uint8_t reserved : 2;
    };

    SecureStream(Stream *stream = nullptr, bool enable = true):
        enabled(enable),
        synced(false),
        block_idx(0),
        read_idx(BLOCK_SIZE),
        write_idx(0),
        base(stream),
        trust(0) {
# ifdef __AVR__
        _init_seed();
# endif /* __AVR__ */
        if (stream != nullptr) {
            stream->setProxyPrint(this);
            stream->setProxyStream(this);
        }
    }
    SecureStream(const SecureStream &) = delete;
    SecureStream &operator=(const SecureStream &) = delete;
    ~SecureStream() {}

    virtual int available();
    virtual int peek();
    virtual int read();
    virtual size_t write(uint8_t);
    virtual size_t write(const uint8_t *, size_t);
    virtual void flush();
    // To overload to do an authentication of the pair from memory:
    virtual bool authenticatePair(const uint8_t *mem, size_t len) { return false; }

    void operator()(Stream &);
    void enable(bool);

    uint8_t getTrustLevel() const { return trust; }

protected:
    virtual bool encrypt(uint8_t *, size_t) = 0;
    virtual bool decrypt(uint8_t *, size_t) = 0;
    virtual void updateKey(uint8_t *, size_t) = 0;
    Stream &getBaseStream() { return *base; }
    size_t _write(const uint8_t *, size_t);

private:
    void updateRX();
    void startOfCommunication();

private:
    bool    enabled;
    bool    synced;
    uint8_t block_idx;
    uint8_t read_idx;
    uint8_t write_idx;
    Stream  *base;
    uint8_t trust;
    uint8_t rx_buffer[BLOCK_SIZE];
    uint8_t read_buffer[BLOCK_SIZE - 1];
};
# endif /* defined(PROXY_PRINT) && PROXY_PRINT == 1 && defined(PROXY_STREAM) && PROXY_STREAM == 1 */
#endif /* SECURE_STREAM_H */