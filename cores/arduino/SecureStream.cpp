#if defined(PROXY_PRINT) && PROXY_PRINT == 1 && defined(PROXY_STREAM) && PROXY_STREAM == 1
# include <string.h>
# include <time.h>
# include "uECC.h"
# include "SecureStream.h"
# include "SecureCommunicationVariables.h"
# include "HardwareSerial.h"

extern "C" {
    unsigned long millis(void);
    void delay(unsigned long);
}

int SecureStream::available() {
    if (base == nullptr) {
        return 0;
    }
    // If cipher isn't enabled, return base implementatiob
    if (!enabled) {
        return base->available();
    }
    // Update receiving buffers
    updateRX();
    // Number of available bytes to read are computed from the position in the buffer
    if (read_idx == write_idx) {
        return 0;
    }
    else if (read_idx < write_idx) {
        return write_idx - read_idx;
    }
    else {
        return (BLOCK_SIZE - 1) - read_idx + write_idx;
    }
}

int SecureStream::peek() {
    if (base == nullptr) {
        return -1;
    }
    if (!enabled) {
        return base->peek();
    }
    updateRX();
    // Return current byte
    return (read_idx < (BLOCK_SIZE - 1)) ? read_buffer[read_idx] : -1;
}

int SecureStream::read() {
    if (base == nullptr) {
        return -1;
    }
    if (!enabled) {
        return base->read();
    }
    updateRX();
    // Return current byte and update buffer position
    if (read_idx != write_idx) {
        int b = read_buffer[read_idx++];
        if (read_idx == BLOCK_SIZE) {
            read_idx = 0; // Move back pointer to the beginning of the circular buffer
        }
        return b;
    }
    else {
        return -1;
    }
}

size_t SecureStream::write(uint8_t b) {
    if (base == nullptr) {
        return 0;
    }
    if (!enabled) {
        return base->write(b);
    }
    updateRX();
    if (!synced) {
        return 0;
    }
    uint8_t buffer[BLOCK_SIZE];
    // Cast first byte as the header
    SecureStream::BlockHeader *header = reinterpret_cast<SecureStream::BlockHeader *>(buffer);
    header->data_length = 1;
    header->protocol_level = PROTOCOL_LEVEL_USER;
    header->key_update = false;
    // Write data in the bloc
    buffer[1] = b;
    // Encrypt bloc
    encrypt(buffer, BLOCK_SIZE);
    // Send bloc
    return (_write(buffer, BLOCK_SIZE)) ? 1 : 0;
}

size_t SecureStream::write(const uint8_t *buffer, size_t len) {
    if (base == nullptr) {
        return 0;
    }
    if (!enabled) {
        return base->write(buffer, len);
    }
    updateRX();
    if (!synced) {
        return 0;
    }
    uint8_t block[BLOCK_SIZE];
    uint8_t *ptr;
    size_t i;
    size_t written = 0;
    // Cast first byte as the header
    SecureStream::BlockHeader *header = reinterpret_cast<SecureStream::BlockHeader *>(block);
    while (len > 0) {
        ptr = block + 1;
        // Write data in the current block
        for (i = 0; i < (BLOCK_SIZE - 1) && i < len; i++) {
            *ptr++ = *buffer++;
            len--;
        }
        // Indicate the number of bytes in the block
        header->data_length = i;
        header->protocol_level = PROTOCOL_LEVEL_USER;
        header->key_update = false;
        // Encrypt block and send it
        if (!encrypt(buffer, BLOCK_SIZE)) {
            return written;
        }
        written += _write(buffer, BLOCK_SIZE);
    }
    return written;
}

/**
 * Reinitializes buffers positions
 */
void SecureStream::flush() {
    synced = false;
    block_idx = 0;
    read_idx = BLOCK_SIZE - 1;
}

/**
 * Configure stream to use indirect calls to this instance of SecureStream
 */
void SecureStream::operator()(Stream &stream) {
    base = &stream;
    synced = false;
    stream.setProxyPrint(this);
    stream.setProxyStream(this);
}

/**
 * Enable or disable encryption
 */
void SecureStream::enable(bool status) {
    enabled = status;
}

void SecureStream::updateRX() {
    if (base == nullptr) {
        return;
    }
    // If not synchronized with the host, instanciate a communication
    if (!synced) {
        startOfCommunication();
    }
    if (synced) {
        // Read bytes from block
        if (block_idx < BLOCK_SIZE) {
            long len = base->readBytes(rx_buffer + block_idx, BLOCK_SIZE - block_idx);
            if (len == -1) {
                return;
            }
            block_idx += len;
        }
        // Decrypt block when complete and copy it into user read buffer
        if (block_idx == BLOCK_SIZE && read_idx == write_idx) {
            block_idx = 0;
            if (decrypt(rx_buffer, BLOCK_SIZE)) {
                SecureStream::BlockHeader *header = reinterpret_cast<BlockHeader *>(rx_buffer);
                if (header->protocol_level == PROTOCOL_LEVEL_USER) {
                    int length = header->data_length;
                    uint8_t *rx = rx_buffer + 1;
                    while (length) {
                        read_buffer[write_idx] = *rx++;
                        length--;
                        if ((write_idx + 1) == read_idx || (write_idx == (BLOCK_SIZE - 1) && !read_idx)) {
                            break;
                        }
                        write_idx++;
                        if (write_idx == BLOCK_SIZE) {
                            write_idx = 0;
                        }
                    }
                }
                if (header->key_update) {
                    updateKey(rx_buffer, BLOCK_SIZE);
                }
            }
        }
    }
}

void SecureStream::startOfCommunication() {
    if (base == nullptr || synced || !enabled) {
        return;
    }
    unsigned long timeout = millis() + getTimeout();
    while (base->available()) {
        if (millis() >= timeout) {
            break;
        }
        {
            uint8_t b[2];
            // Look if first incoming byte is the magic byte to begin communication
            if (base->readBytes(b, 1) < 1 || b[0] != SYNC_PATTERN) {
                continue; // Throw away all bytes not related to beginning of a communication
            }
        }
        {
            uint8_t key[65];
            uint8_t privKey[32];
            uint8_t secret[32];
            // Get private key from program memory
            getPrivateKey(privKey);
            // Get public key from program memory
            getPublicKey(key);
            // Send public key to host
            for (uint8_t i = 0; i < 65; i++) {
                base->write(key[i]);
                delay(10);
            }
            uint8_t idx = 0;
            long len;
            base->read();
            // Read public key from host
            while (idx < 64) {
                if ((len = base->readBytes(key + idx, 64 - idx)) > 0) {
                    idx += len;
                }
            }
            // Generate ECDH secret
            uECC_Curve curve = uECC_secp256k1();
            uECC_shared_secret(key, privKey, secret, curve);
            // Update key from secret, using BLOCK_SIZE bytes
            updateKey(secret, BLOCK_SIZE);
            synced = true;
        }
    }
}

size_t SecureStream::_write(const uint8_t *buffer, size_t len) {
    size_t i = 0;
    while (len--) {
        if (base->write(*buffer++)) {
            i++;
        }
        else {
            break;
        }
    }
    return i;
}
#endif /* defined(PROXY_PRINT) && PROXY_PRINT == 1 && defined(PROXY_STREAM) && PROXY_STREAM == 1 */