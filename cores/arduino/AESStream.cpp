#if defined(PROXY_PRINT) && PROXY_PRINT == 1 && defined(PROXY_STREAM) && PROXY_STREAM == 1
# include <stdlib.h>
# include <string.h>
# include "AESStream.h"

AESStream::AESStream(Stream *stream, bool enable):
    initialized(false),
    SecureStream(stream, enable) {

}

AESStream::~AESStream() {}

bool AESStream::encrypt(uint8_t *buffer, size_t size) {
    if (!initialized) {
        return false;
    }
    AES_CTR_xcrypt_buffer(&tx_ctx, buffer, size);
    return true;
}

bool AESStream::decrypt(uint8_t *buffer, size_t size) {
    if (!initialized) {
        return false;
    }
    AES_CTR_xcrypt_buffer(&rx_ctx, buffer, size);
    return true;
}

void AESStream::updateKey(uint8_t *mem, size_t len) {
    if (len < 16) {
        return;
    }
    uint8_t iv[16];
    for (uint8_t i = 0; i < 16; i++) {
        iv[i] = rand() % 256;
    }
    _write(iv, sizeof(iv));
    AES_init_ctx_iv(&rx_ctx, mem, iv);
    memcpy(&tx_ctx, &rx_ctx, sizeof(AES_ctx));
    initialized = true;
}
#endif