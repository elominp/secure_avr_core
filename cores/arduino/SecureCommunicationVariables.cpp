#include "SecureCommunicationVariables.h"

#ifdef SECURE_COMMUNICATION_PSK
int getEncryptionKey(uint8_t *dest) {
# ifdef __AVR__
    // Copy bytes from program memory
    PGM_VOID_P key = reinterpret_cast<PGM_VOID_P>(_encryption_key);
    for (size_t i = 0; i < 16; i++) {
        *dest++ = pgm_read_byte(key++);
    }
# else
    // Copy bytes from data memory
    const uint8_t *key = _encryption_key;
    for (size_t i = 0; i < 16; i++) {
        *dest++ = *key++;
    }
# endif /* __AVR__ */
    return 0;
}
#endif /* SECURE_COMMUNICATION_PSK */

int getPublicKey(uint8_t *dest) {
#ifdef __AVR__
    // Copy bytes from program memory
    PGM_VOID_P pubKey = reinterpret_cast<PGM_VOID_P>(_encryption_public_key);
    for (size_t i = 0; i < 65; i++) {
        *dest++ = pgm_read_byte(pubKey++);
    }
#else
    // Copy bytes from data memory
    const uint8_t *pubKey = _encryption_public_key;
    for (size_t i = 0; i < 65; i++) {
        *dest++ = *pubKey++;
    }
#endif
    return 0;
}

int getPrivateKey(uint8_t *dest) {
#ifdef __AVR__
    // Copy bytes from program memory
    PGM_VOID_P privKey = reinterpret_cast<PGM_VOID_P>(_encryption_private_key);
    for (size_t i = 0; i < 32; i++) {
        *dest++ = pgm_read_byte(privKey++);
    }
#else
    // Copy bytes from data memory
    const uint8_t *privKey = _encryption_private_key;
    for (size_t i = 0; i < 32; i++) {
        *dest++ = *privKey++;
    }
#endif /* __AVR__ */
    return 0;
}