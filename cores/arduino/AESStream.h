#ifndef AESSTREAM_H
# define AESSTREAM_H
# if defined(PROXY_PRINT) && PROXY_PRINT == 1 && defined(PROXY_STREAM) && PROXY_STREAM == 1
#  include "SecureStream.h"
#  include "aes.hpp"

class AESStream : public SecureStream {
public:
    AESStream(Stream *stream = nullptr, bool enable = true);
    AESStream(const AESStream &) = delete;
    AESStream &operator=(const AESStream &) = delete;
    ~AESStream();
protected:
    virtual bool encrypt(uint8_t *, size_t);
    virtual bool decrypt(uint8_t *, size_t);
    virtual void updateKey(uint8_t *, size_t);
private:
    typedef struct AES_ctx AES_ctx;
    bool    initialized;
    AES_ctx rx_ctx;
    AES_ctx tx_ctx;
};
# endif /* defined(PROXY_PRINT) && PROXY_PRINT == 1 && defined(PROXY_STREAM) && PROXY_STREAM == 1 */
#endif /* AESSTREAM_H */